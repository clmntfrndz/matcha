/* global $, io */
let socket = io();

$(document).ready(function() {
	// Check for click events on the navbar burger icon
	$('.navbar-burger').click(function() {

		// Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
		$('.navbar-burger').toggleClass('is-active');
		$('.navbar-menu').toggleClass('is-active');
	});
});

const rootElem = $(document.documentElement);
const modals = $('.modal');

const modalCloses = $(
	'.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button');
const modalButtons = $('.modal-button');

function openModal(target) {
	rootElem.addClass('is-clipped');
	$(target).addClass('is-active');
}

function closeModals() {
	rootElem.removeClass('is-clipped');
	modals.removeClass('is-active');
}

modalCloses.on('click', closeModals);

modalButtons.each((index, el) => {
	$(el).on('click', () => {
		const target = el.dataset.target;

		openModal(target);
	});
});

$('#read-notifs').on('click', () => {
	$.post('/user/notif/read-all').done((result) => {
		refreshNotifications(result.notifs);
	});
});

$('.tag.is-delete').on('click', (e) => {
	$.post('/user/tags/delete', { userTagId: e.target.id }).done(() => {
		window.location.replace('/user/edit');
	}).fail(() => {
		alert('Error while deleting tag.');
	});
});

// User pictures on /user/edit
$('.user-edit figure').on('click', (e) => {
	let formId = e.currentTarget.dataset.target;

	$('#form-' + formId).find('input[type="file"]').click();
});

$('input[type="file"]').on('change', (e) => {
	// console.log('file input changed: ', e);
	$(e.target).next().click();
});

socket.on('chat message', (msg) => {
	// console.log('msg = ', msg);
	if (window.location.pathname.indexOf('/user/chat') === 0) {
		insertMsg(msg);
	}
});

function insertMsg(msg) {
	$('.user-messages').append(`<div>${msg.message}</div>`);
}

$('#chat-submit').on('click', () => {
	const message = $('#chat-input').val();
	const userTo = window.location.pathname.split('/').pop();
	const msg = {
		message
		// fromUsername
	};

	insertMsg(msg);
	// console.log('message = ', message);
	$.post('/user/chatmsg', { message, userTo });
	$('#chat-input').val('');
});

function getLocation() {
	let location;

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(onSuccess, onErr);
	} else {
		throw new Error('geolocation not enabled');
	}

	function onSuccess(pos) {
		let latlng = {
			lat: pos['coords']['latitude'],
			lng: pos['coords']['longitude']
		};
		location = pos;
		geocodeLatLng(latlng);
	}

	function onErr(err) {
		// console.log('user refused geoloc', err);
	}
}

function geocodeLatLng(latLng) {
	var geocoder = new google.maps.Geocoder;

	geocoder.geocode({ 'location': latLng }, (results, status) => {
		if (status === 'OK') {
			// console.log('results', results, latLng);
			if (results[0]) {
				let locationFromLatLng = _.find(results, (addr) => {
					if (addr.types[0] === 'postal_code')
						return true;
					return false;
				});
				let location = locationFromLatLng['formatted_address'];

				$.post('/user/update-location',
					{ lat: latLng['lat'], lng: latLng['lng'], location });
			} else {
				window.alert('No results found');
			}
		} else {
			window.alert('Geocoder failed due to: ' + status);
		}
	});
}

if (location.search.indexOf('connect=true') > 0)
	getLocation();

function refreshNotifications(notifs) {
	$('.notifs-count').text(notifs.length);

	$('.notifs').empty();
	notifs.forEach((notif) => {
		const html = [
			`<a href="/user/${notif.userFrom}" class="dropdown-item">`,
			`<div><b>${notif.username}</b> ${notif.text}</div>`,
			`<em>on ${notif.formattedDate}</em>`,
			`</a>`
		].join('');

		$('.notifs').append('<hr class="dropdown-divider">').append(html);
	});
}

socket.on('notifs', (data) => {
	// console.log('refrsh notifs: ', data.notifs);
	refreshNotifications(data.notifs);
});

if (location.search) {
	let params = location.search.split('&');
	let inputs = [
		'min-age',
		'max-age',
		'min-popularity',
		'max-popularity',
		'location'
	];

	params.forEach((param) => {
		inputs.forEach((input) => {
			if (param.indexOf(input) !== -1) {
				let value = param.split('=')[1];

				$(`[name="${input}"]`).val(value);
			}
		});
	});
}

// Use this instead of () => {} because we need this to refer to the select
$('select[name="sort"]').on('change', function() {
	// console.log('this = ', this);
	if (!location.search)
		location.search = `?sort=${this.value}`;
	else {
		if (location.search.indexOf('sort') === -1)
			location.search += `&sort=${this.value}`;
		else {
			let params = location.search.split('&');

			params.forEach((param, index) => {
				if (param.indexOf('sort') !== -1) {
					if (param[0] === '?')
						params[index] = `?sort=${this.value}`;
					else
						params[index] = `sort=${this.value}`;
				}
			});
			// console.log('location search =', params.join('&'));
			location.search = params.join('&');
		}
	}
});

$('#search-tags-input #add-button').on('click', function() {
	let tagText = $('#search-tags-input input').val();
	let deleteButton = $(
		`<div class="tag is-delete is-medium">
		</div>`
	).on('click', function() {
		// console.log('click on this:', $(`[data-tag-text=${tagText}]`));
		$(`[data-tag-text=${tagText}]`).remove();
	});

	$('#search-tags')
		.append(`<div class="tags has-addons" data-tag-text="${tagText}"></div>`);
	$(`[data-tag-text=${tagText}]`)
		.append(`<div class="tag is-primary is-medium">#${tagText}</div>`)
		.append(deleteButton);
});
