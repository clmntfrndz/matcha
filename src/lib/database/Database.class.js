class Database {
	constructor(opts) {
		if (typeof opts !== 'object')
			throw new Error(`Database constructor expected object, got ${opts}`);
		this.connection = opts.connection;
	}

	/**
	 * Release a connection pool
	 * @returns {Promise<any>}
	 */
	release() {
		return new Promise((resolve, reject) => {
			this.connection.release((err) => {
				if (err)
					return reject(err);
				resolve();
			});
		});
	}

	query(sql, params) {
		return new Promise((resolve, reject) => {
			this.connection.query(sql, params, (err, rows) => {
				if (err)
					return reject(err);
				resolve(rows);
			});
		});
	}
}

module.exports = Database;
