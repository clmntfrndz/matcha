const mysql = require('mysql');
const Database = require('./Database.class');
const config = require('../../config.json');

const pool = mysql.createPool({
	host: config.db.host,
	port: config.db.port,
	user: config.db.credentials.user,
	password: config.db.credentials.passwd,
	database: config.db.name,
	connectionLimit: 10,
});

function getConnection() {
	return new Promise((resolve, reject) => {
		pool.getConnection((err, connection) => {
			if (err)
				return reject(err);
			const db = new Database({ connection });
			resolve(db);
		});
	});
}

module.exports = {
	getConnection,
};
