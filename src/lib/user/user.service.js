const User = require('./User.class');
const dbService = require('../database/database.service');
const debug = require('debug')('matcha:user.service');
const _ = require('lodash');
const crypto = require('../crypto.service');
const moment = require('moment');
const socketApi = require('../../socketApi');
const request = require('request-promise');
const mailer = require('../mailer.service');

function getMenu() {
	return [
		{
			name: 'Let\'s go!',
			items: [
				{
					href: '/user/matches',
					text: 'Your matches',
					icon: 'fas fa-heart'
				},
				{
					href: '/user/search',
					text: 'Find new people',
					icon: 'fas fa-search'
				},
				{
					href: '/user/likes',
					text: 'They liked you',
					icon: 'fas fa-thumbs-up'
				},
				{
					href: '/user/visits',
					text: 'They visited your profile',
					icon: 'fas fa-eye'
				}
			]
		},
		{
			name: 'Administration',
			items: [
				{
					href: '/user/me',
					text: 'View your profile',
					icon: 'fas fa-crown'
				},
				{
					href: '/user/edit',
					text: 'Edit your profile',
					icon: 'fas fa-edit'
				}
			]
		}
	];
}

/**
 * Return the menu for the user layout.
 * Find and set active property on the corresponding menu item (based on the url
 * found in req.originalUrl).
 * @param {string} url URL requested by the user
 * @returns {array} The menu
 */
function menu(url) {
	const m = getMenu();
	const activeSubmenu = _.find(m, { items: [{ href: url }] });
	let activeItem;

	if (activeSubmenu) {
		activeItem = _.find(activeSubmenu.items, { href: url });
		activeItem.active = true;
	}
	return m;
}

async function getByUsername(username) {
	debug(`getByUsername: ${username}`);
	const db = await dbService.getConnection();

	try {
		const rows = await db.query('SELECT * FROM `User` WHERE `username` = ?',
			[username]);
		let user;

		if (!rows.length)
			return null;
		user = new User(rows[0]);
		user.tags = await loadTags(user.id);
		return user;
	}
	finally {
		db.release();
	}
}

//
async function getById(id) {
	// debug(`getById: ${id}`);
	const db = await dbService.getConnection();

	try {
		const rows = await db.query('SELECT * FROM User WHERE id = ?', [id]);
		let user;

		if (!rows.length)
			return null;
		user = new User(rows[0]);
		return user;
	}
	finally {
		db.release();
	}
}

async function getByEmail(email) {
	debug(`getByEmail: ${email}`);
	const db = await dbService.getConnection();

	try {
		const rows = await db.query('SELECT * FROM User WHERE email = ?', [email]);
		let user;

		if (!rows.length)
			return null;
		user = new User(rows[0]);
		return user;
	}
	finally {
		db.release();
	}
}

async function getByPasswdResetCode(code) {
	debug(`getByPasswdResetCode: ${code}`);
	const db = await dbService.getConnection();
	const sql = 'SELECT * FROM User WHERE resetPasswd = ?';

	try {
		const rows = await db.query(sql, [code]);
		let user;

		if (!rows.length)
			return null;
		user = new User(rows[0]);
		return user;
	}
	finally {
		db.release();
	}
}

async function getByActivationCode(code) {
	const db = await dbService.getConnection();
	const sql = 'SELECT * FROM User WHERE activationCode = ?';

	try {
		const rows = await db.query(sql, [code]);

		if (!rows.length)
			return null;
		return new User(rows[0]);
	}
	finally {
		db.release();
	}
}

async function login(username, passwd) {
	let user = await getByUsername(username);

	if (user) {
		debug('Found user: ', user);
		if (await user.auth(passwd))
			return user;
	}
	return null;
}

/**
 *
 * @param {number} userId    User id for which we will fetch tags
 * @returns {Promise<void>}
 */
async function loadTags(userId) {
	const db = await dbService.getConnection();
	const queryParams = [userId];

	debug('loadTags', userId);

	try {
		let sql = [
			'SELECT',
			'UserTag.id AS userTagId,',
			'Tag.name AS tagName,',
			'Tag.id AS tagId',
			'FROM Tag',
			'INNER JOIN UserTag',
			'ON Tag.id = UserTag.tagId',
			'WHERE UserTag.userId = ?',
			'ORDER BY UserTag.id'
		].join(' ');

		return await db.query(sql, queryParams);
	} finally {
		db.release();
	}
}

/**
 * Create a new UserTag and the corresponding Tag if it doesn't exist yet.
 * @param {Object} tag                   Object representing the new UserTag
 * @param {string} tag.name     Tag content
 * @param {number} tag.userId   User id for the new UserTag (found in session)
 * @returns {Promise<void>}
 */
async function addTag(tag) {
	const db = await dbService.getConnection();
	try {
		const rows = await db.query('SELECT * FROM `Tag` WHERE `name` = ?',
			tag.name);
		let tagId;
		let userTag;

		if (!rows.length) {
			const res = await db.query('INSERT INTO `Tag` SET ?', { name: tag.name });

			tagId = res.insertId;
		} else {
			tagId = rows[0].id;
		}

		// Preare UserTag record
		userTag = {
			tagId,
			userId: tag.userId
		};

		await db.query('INSERT Into `UserTag` SET ?', userTag);
	}
	finally {
		db.release();
	}
}

/**
 * Delete a UserTag record (associated with a User and a Tag).
 * @param {Object} tag             Id of the UserTag
 * @param {number} tag.userTagId   If of the UserTag record
 * @param {number} tag.userId      User id (found in the session)
 * @returns {Promise<void>}
 */
async function deleteTag(tag) {
	const db = await dbService.getConnection();
	const params = [tag.userId, tag.userTagId];

	debug('delete tag: ', tag);
	try {
		await db.query('DELETE FROM `UserTag` WHERE userId = ? AND id = ?', params);
	}
	finally {
		db.release();
	}
}

async function register(userData, hostname) {
	const db = await dbService.getConnection();
	const mail = [
		'Your account was successfully created.<br>',
		'Please activate it by clicking this link:',
		`http://${hostname}:3000/activate?code=${userData.activationCode}`
	].join('<br>');

	userData['passwdHash'] = await crypto.hash(userData['passwd']);
	delete userData['passwd'];
	userData['birthdate'] = moment(userData['date'], `YYYY-MM-DD`)
		.format('YYYY-MM-DD');
	delete userData['date'];
	try {
		await db.query('INSERT INTO `User` SET ?', userData);
		await sendMail(userData.email, 'Account activation', mail);
	}
	finally {
		db.release();
	}
}

async function updateProfile(userData) {
	const db = await dbService.getConnection();
	const id = userData.id;

	try {
		await db.query('UPDATE `User` SET ? WHERE id= ?', [userData, id]);
	}
	finally {
		db.release();
	}
}

async function uploadImage(imageData) {
	const db = await dbService.getConnection();
	const image = {
		path: imageData.path,
		userId: imageData.userId,
		imageIndex: imageData.imageIndex
	};
	const params = [image, image.path];
	const getImages = 'SELECT COUNT(*) AS count FROM `Image` WHERE userId = ?';
	const getImagesParams = [imageData.userId];

	console.log(`image.userId : ${image.userId} \n\n\n`);
	try {
		let res = await db.query(getImages, getImagesParams);

		console.log('res[0].count', res[0].count);
		if (!res[0].count)
			image.isProfile = 1;

		await db.query('INSERT INTO `Image` SET ? ON DUPLICATE KEY UPDATE path = ?',
			params);
	}
	catch (err) {
		if (err.code === 'ER_DUP_ENTRY')
			return;
		throw err;
	}
	finally {
		db.release();
	}
}

async function editPasswd(userId, newPasswd) {
	const db = await dbService.getConnection();
	const newHash = await crypto.hash(newPasswd);

	try {
		await db.query('UPDATE `User` SET ? WHERE id = ?',
			[{ passwdHash: newHash }, userId]);
	}
	finally {
		db.release();
	}
}

async function editEmail(oldEmail, newEmail) {
	const db = await dbService.getConnection();

	try {
		await db.query('UPDATE `User` SET ? WHERE email= ?',
			[{ email: newEmail }, oldEmail]);
	}
	finally {
		db.release();
	}
}

async function getImages(userId) {
	const db = await dbService.getConnection();

	try {
		return await db.query('SELECT * FROM `Image` WHERE userId = ?', [userId]);
	}
	finally {
		db.release();
	}
}

async function deleteImage(userId, imageIndex) {
	const db = await dbService.getConnection();

	try {
		return await db.query(
			'DELETE FROM `Image` WHERE userId = ? AND imageIndex = ? AND isProfile != 1',
			[userId, imageIndex]);
	}
	finally {
		db.release();
	}
}

async function addVisit(userFrom, userTo) {
	debug(`addVisit from ${userFrom} to ${userTo}`);
	const db = await dbService.getConnection();
	const visitData = {
		userFrom,
		userTo,
		date: moment().format('YYYY-MM-DD HH:mm:ss'),
		count: 1
	};

	try {
		await db.query(
			'INSERT INTO `UserVisit` SET ? ON DUPLICATE KEY UPDATE date = ?, count = count + 1 ',
			[visitData, visitData.date]);
		await updatePopularity(userTo);
		await addNotification(userFrom, userTo, 'VISIT');
	}
	catch (err) {
		debug('Error caught during addVisit :', err.message);
		throw new Error(err);
	}
	finally {
		db.release();
	}
}

async function getVisits(userId) {
	const db = await dbService.getConnection();
	const sql = [
		'SELECT `User`.id, `User`.username, `UserVisit`.date, `UserVisit`.count',
		'FROM `UserVisit`',
		'INNER JOIN `User` ON `User`.id = `UserVisit`.userFrom',
		'WHERE `UserVisit`.userTo = ?'
	].join(' ');
	const params = [userId];

	try {
		let visits = await db.query(sql, params);

		visits.forEach((visit) => {
			visit.formattedDate = moment(visit.date).format('LLL');
		});
		return visits;
	}
	catch (err) {
		debug('Error caught during getVisits :', err.message);
		throw new Error(err);
	}
	finally {
		db.release();
	}
}

async function addLike(userFrom, userTo) {
	const db = await dbService.getConnection();
	const likeData = {
		userFrom,
		userTo,
		date: moment().format('YYYY-MM-DD HH:mm:ss')
	};

	try {
		await db.query(
			'INSERT INTO `UserLike` SET ? ON DUPLICATE KEY UPDATE `date` = ?',
			[likeData, likeData.date]);

		// Update popularity score (match/visits)
		await updatePopularity(userTo);

		// Create notification, either first like or match (likeback)
		if (await matched(userFrom, userTo))
			await addNotification(userFrom, userTo, 'LIKEBACK');
		else
			await addNotification(userFrom, userTo, 'LIKE');
	}
	finally {
		db.release();
	}
}

async function getLikers(userId) {
	const db = await dbService.getConnection();
	const sql = [
		'SELECT `User`.id, `User`.username, `User`.calcLocation, `User`.city, `UserLike`.date',
		'FROM `UserLike`',
		'INNER JOIN `User` ON `User`.id = `UserLike`.UserFrom',
		'WHERE `UserLike`.userTo = ?'
	].join(' ');
	const params = [userId];

	try {
		let likers = await db.query(sql, params);

		likers.forEach((liker) => {
			liker.formattedDate = moment(liker.date).format('LLL');
		});
		return likers;
	}
	catch (err) {
		debug(`Err caught in getLikers (userId: ${userId})`);
		throw err;
	}
	finally {
		db.release();
	}
}

async function unLike(userFrom, userTo) {
	const db = await dbService.getConnection();

	try {
		const res = await db.query(
			'DELETE FROM `UserLike` WHERE `userFrom` = ? AND `userTo` = ?',
			[userFrom, userTo]);
		await updatePopularity(userTo);
		await addNotification(userFrom, userTo, 'UNLIKE');
		return res;
	}
	finally {
		db.release();
	}
}

async function getMsg(userFrom, userTo) {
	debug('getMsg from: ' + userFrom + ' to ' + userTo);
	const db = await dbService.getConnection();
	const sql = [
		'SELECT userFrom, userTo, date, content FROM `UserMessage`',
		'WHERE (`userFrom` = ? AND `userTo` = ?)',
		'OR (`userFrom` = ? AND `userTo` = ?)'
	].join(' ');
	const params = [userFrom, userTo, userTo, userFrom];

	try {
		return await db.query(sql, params);
	}
	finally {
		db.release();
	}
}

async function sendMsg(userFrom, userTo, content) {
	const db = await dbService.getConnection();
	const msgData = {
		userFrom,
		userTo,
		date: moment().format('YYYY-MM-DD HH:mm:ss'),
		content
	};

	try {
		await db.query('INSERT INTO `UserMessage` SET ?', [msgData]);
		await addNotification(userFrom, userTo, 'MSG');
	}
	finally {
		db.release();
	}
}

async function getMatches(userId) {
	const db = await dbService.getConnection();
	const params = [userId, userId];
	const sql = [
		'SELECT `User`.*',
		'FROM `User`',
		'INNER JOIN',
		'  ( SELECT userIdB AS matchId',
		'  FROM `Match`',
		'  WHERE `Match`.userIdA = ?',
		'  UNION SELECT userIdA AS matchId',
		'  FROM `Match`',
		'  WHERE `Match`.userIdB = ? )',
		'AS `UserMatches` ON `UserMatches`.matchId = `User`.id'
	].join(' ');

	try {
		return db.query(sql, params);
	}
	finally {
		db.release();
	}
}

async function matched(first, second) {
	const matches = await getMatches(first);

	if (!_.find(matches, { id: parseInt(second) }))
		return false;
	return true;
}

async function isLiked(from, to) {
	const db = await dbService.getConnection();
	const sql = 'SELECT * FROM UserLike WHERE UserFrom = ? AND UserTo = ?';
	const params = [from, to];

	try {
		let result = await db.query(sql, params);

		return !!result.length;
	}
	finally {
		db.release();
	}
}

// TODO : fix when there is no oldProfile
async function setProfileImage(userId, imageIndex) {
	const db = await dbService.getConnection();
	const params = [userId, userId, imageIndex];
	const sql = [
		'UPDATE `Image` oldProfile, `Image` newProfile',
		'SET oldProfile.isProfile = 0, newProfile.isProfile = 1',
		'WHERE oldProfile.isProfile = 1',
		'AND oldProfile.userId = ?',
		'AND newProfile.userId = ? AND newProfile.imageIndex = ?'
	].join(' ');

	try {
		return db.query(sql, params);
	}
	finally {
		db.release();
	}
}

async function updatePopularity(userId) {
	const db = await dbService.getConnection();
	let params = [userId, userId];
	let sql = [
		'SELECT ( SELECT COUNT(*) FROM `UserVisit` WHERE userTo = ? )',
		'AS `visitsCount`,',
		'( SELECT COUNT(*) FROM `UserLike` WHERE userTo = ? )',
		'AS `likesCount`'
	].join(' ');

	try {
		const res = await db.query(sql, params);
		console.log(res);
		const popularity = (parseInt(res[0].likesCount) * 100) /
			parseInt(res[0].visitsCount);
		console.log(popularity);
		sql = 'UPDATE `User` SET `popularity` = ? WHERE `id` = ?';
		params = [popularity, userId];
		await db.query(sql, params);
	}
	finally {
		db.release();
	}
}

function getOrientationMScore(userA, userB) {
	if (userA.gender === `X` || userB.gender === `X`)
		return 5;
	switch (userA.orientation) {
		case `hetero` :
			if (userA.gender === `M`) {
				if (userB.gender === `F` && userB.orientation === `hetero`)
					return 20;
				if (userB.gender === `F` && userB.orientation === `bisexual`)
					return 10;
			}
			if (userA.gender === `F`) {
				if (userB.gender === `M` && userB.orientation === `hetero`)
					return 20;
				if (userB.gender === `M` && userB.orientation === `bisexual`)
					return 10;
			}
			return 0;

		case `gay` :
			if (userA.gender === `M`) {
				if (userB.gender === `M` && userB.orientation === `gay`)
					return 20;
				if (userB.gender === `M` && userB.orientation === `bisexual`)
					return 10;
			}
			if (userA.gender === `F`) {
				if (userB.gender === `F` && userB.orientation === `gay`)
					return 20;
				if (userB.gender === `F` && userB.orientation === `bisexual`)
					return 10;
			}
			return 0;

		case `bisexual` :
			if (userA.gender === `M`) {
				if ((userB.gender === `F` || userB.gender === `M`) &&
					userB.orientation === `bisexual`)
					return 20;
				if (userB.gender === `F` && userB.orientation === `hetero`)
					return 15;
				if (userB.gender === `M` && userB.orientation === `gay`)
					return 15;
			}
			if (userA.gender === `F`) {
				if ((userB.gender === `M` || userB.gender === `F`) &&
					userB.orientation === `bisexual`)
					return 20;
				if (userB.gender === `M` && userB.orientation === `hetero`)
					return 15;
				if (userB.gender === `F` && userB.orientation === `gay`)
					return 15;
			}
			return 0;
	}
}

function getPopularityMScore(userA, userB) {
	const diff = Math.abs(userA.popularity - userB.popularity);

	if (userA.popularity === userB.popularity)
		debug('equal pop for: ', userA, userB);

	if (diff < 5)
		return 20;
	if (diff < 20)
		return 15;
	if (diff < 45)
		return 10;
	if (diff < 65)
		return 5;
	return 0;
}

async function getInterestMScore(userA, userB) {
	const db = await dbService.getConnection();
	let matchPercent = 0;
	let params = [userA.id, userB.id];
	let sql = [
		'SELECT UserTagA.*,',
		'UserTagB.*',
		'FROM UserTag UserTagA',
		'INNER JOIN UserTag UserTagB ON UserTagA.tagId = UserTagB.tagId',
		'WHERE UserTagA.userId = ?',
		'AND UserTagB.userId = ?'
	].join(' ');

	try {
		let res = await db.query(sql, params);

		if (res.length > 2)
			return 30;
		if (res.length === 2)
			return 20;
		if (res.length === 1)
			return 10;
		return matchPercent;
	}
	finally {
		db.release();
	}
}

async function getLocationMScore(userA, userB) {
	const locationA = userA.calcLocation || userA.city;
	const locationB = userB.calcLocation || userB.city;
	const distance = await getDistance(locationA, locationB);

	// Error while looking up distance via google apis
	if (distance < 0)
		return 0;

	if (distance < 5000)
		return 30;
	if (distance < 10000)
		return 25;
	if (distance < 15000)
		return 20;
	if (distance < 20000)
		return 15;
	if (distance < 25000)
		return 10;
	if (distance < 50000)
		return 5;
	return 0;
}

/**
 * Returns the distance between two locations if present in the cache or -1
 * @param locationA
 * @param locationB
 * @returns {Promise<Number>} Distance between locations if found, -1 otherwise
 */
async function queryDistanceCache(locationA, locationB) {
	const db = await dbService.getConnection();
	const sql = [
		'SELECT * FROM DistanceCache',
		'WHERE (locationFrom = ? AND locationTo = ?)',
		'OR (locationFrom = ? AND locationTo = ?)'
	].join(' ');
	const params = [locationA, locationB, locationB, locationA];

	try {
		let rows = await db.query(sql, params);

		if (!rows.length)
			return -1;
		debug(`cache hit for ${locationA} and ${locationB}`);
		return rows[0].distance;
	}
	finally {
		db.release();
	}
}

async function cacheDistance(locationA, locationB, distance) {
	const db = await dbService.getConnection();
	const sql = 'INSERT INTO `DistanceCache` SET ?';
	const params = {
		locationFrom: locationA,
		locationTo: locationB,
		distance
	};

	debug('cacheDistance', params);
	try {
		return db.query(sql, [params]);
	}
	finally {
		db.release();
	}
}

/**
 * Makes a request to google apis to get the distance between 2 cities,
 * used to calculate location score based on distance (in meters)
 * @param userA User
 * @param userB User instance
 * @returns {Promise<Number>}
 */
async function getDistance(locationA, locationB) {
	const api = {
		key: 'AIzaSyC0xT642auwScQUJuNxtGI1IqA8IGf5tdc',
		host: 'https://maps.googleapis.com',
		path: '/maps/api/distancematrix/json'
	};
	const qs = {
		origins: locationA,
		destinations: locationB,
		key: api.key
	};
	const uri = api.host + api.path;

	debug(`get distance ${locationA} ${locationB}`);

	try {
		let cacheHit = await queryDistanceCache(locationA, locationB);

		// If both locations were found in cache, don't query google apis
		if (cacheHit >= 0)
			return cacheHit;

		// Otherwise ask google for the distance and cache the result
		let result = await request({ uri, qs, json: true });

		if (result.status === 'OK' && result.rows[0].elements[0].status === 'OK') {
			const distance = result.rows[0].elements[0].distance['value'];

			await cacheDistance(locationA, locationB, distance);
			return distance;
		}
		return -1;
	}
	catch (err) {
		debug('Error caught during getDistance: ', err);
		throw err;
	}
}

async function getMatchScore(userA, uIdB) {
	// const userA = await getById(uIdA);
	const userB = await getById(uIdB);
	const orientationScore = getOrientationMScore(userA, userB);

	if (orientationScore === 0)
		return null;

	let matchScore = {
		orientation: orientationScore,
		popularity: getPopularityMScore(userA, userB),
		interest: await getInterestMScore(userA, userB),
		location: await getLocationMScore(userA, userB)
	};
	// matchScore.total = parseInt(matchScore.orientationScore +
	// matchScore.popularityScore + matchScore.interestScore
	// + matchScore.locationScore);
	// equivalent to :
	matchScore.total = Object.values(matchScore).reduce((a, b) => a + b);
	return matchScore;
}

async function updateLocation(userId, latLng, locFromLatLng) {
	const db = await dbService.getConnection();
	const data = {
		lat: latLng['lat'],
		lng: latLng['lng'],
		calcLocation: locFromLatLng
	};
	const sql = [
		'UPDATE `User` SET ? WHERE id = ?'
	].join(' ');

	debug('sql = ', sql, 'data= ', data);

	try {
		return db.query(sql, [data, userId]);
	}
	finally {
		db.release();
	}
}

/**
 * @private
 * @param userFrom The user who triggered the new notif
 * @param userTo Target user (liked, unliked, visited..)
 * @param type LIKE | VISIT | MSG | LIKEBACK | UNLIKE
 * @returns {Promise<*>}
 */
async function addNotification(userFrom, userTo, type) {
	// check for blocked users
	const blockedList = await getBlockedList(userTo);
	if (blockedList.indexOf(userFrom) !== -1)
		return ;
	const db = await dbService.getConnection();
	const sql = [
		'INSERT INTO `Notification` SET ?'
	].join(' ');
	const notifData = {
		userFrom,
		userTo,
		type,
		date: moment().format('YYYY-MM-DD HH:mm:ss')
	};
	const sockets = socketApi.getUserSockets(userTo);

	debug('Insert notif: ', notifData);
	try {
		await db.query(sql, [notifData]);
		let notifs = await getNotifications(userTo);

		if (sockets) {
			Object.keys(sockets).forEach((id) => {
				sockets[id].emit('notifs', { notifs });
			});
		}
	}
	catch(err) {
		debug('Error caught during addNotification', err);
		throw err;
	}
	finally {
		db.release();
	}
}

async function getNotifications(userId) {
	const db = await dbService.getConnection();
	const sql = [
		'SELECT `User`.id as userFrom,',
		'`User`.`username`,',
		'`Notification`.type,',
		'`Notification`.date',
		'FROM `Notification`',
		'INNER JOIN `User` ON `User`.id = `Notification`.userFrom',
		'WHERE `Notification`.userTo = ? AND `Notification`.seen IS NULL',
		'ORDER BY `Notification`.date DESC'
	].join(' ');
	const params = [userId];

	try {
		let notifs = await db.query(sql, params);

		notifs.forEach((notif) => {
			switch (notif.type) {
				case 'LIKE':
					notif.text = 'liked you.';
					break;
				case 'VISIT':
					notif.text = 'visited your profile.';
					break;
				case 'MSG':
					notif.text = 'sent you a message.';
					break;
				case 'LIKEBACK':
					notif.text = 'liked you back, it\'s a match!';
					break;
				case 'UNLIKE':
					notif.text = 'unliked you.';
					break;
			}
			notif.formattedDate = moment(notif.date).format('LLL');
		});
		return notifs;
	}
	finally {
		db.release();
	}
}

async function readNotifications(userId) {
	const db = await dbService.getConnection();
	const sql = [
		'UPDATE `Notification` SET `Notification`.seen = TRUE',
		'WHERE `Notification`.userTo = ?'
	].join(' ');
	const params = [userId];

	try {
		return db.query(sql, params);
	}
	finally {
		db.release();
	}
}

async function getAll() {
	const db = await dbService.getConnection();

	try {
		return db.query('SELECT * FROM `User`');
	}
	finally {
		db.release();
	}
}

async function blockUser(userFrom, userTo) {
	const db = await dbService.getConnection();
	const sql = 'INSERT INTO `BlockedUser` SET ?';
	const param = {
		userFrom,
		userTo
	};

	try {
		return db.query(sql, [param]);
	}
	finally {
		db.release();
	}
}

async function reportUser(userTo) {
	const db = await dbService.getConnection();
	const sql = 'INSERT INTO `ReportedUser` SET ? ' +
		'ON DUPLICATE KEY UPDATE reports = reports + 1';
	const param = {
		userTo: userTo,
		reports: 1
	};

	try {
		return db.query(sql, [param]);
	}

	finally {
		db.release();
	}
}

async function getBlockedList(userId) {
	const db = await dbService.getConnection();
	const sql = 'SELECT `userTo` FROM `BlockedUser` WHERE `userFrom` = ?';

	try {
		let list = await db.query(sql, [userId]);

		return list.map(elem => {
			return elem.userTo;
		});
	}
	finally {
		db.release();
	}
}

async function listFilter(list, userId) {
	const blockedList = await getBlockedList(userId);
	blockedList.push(userId);
	_.remove(list, (user) => {
		return blockedList.indexOf(user.id) !== -1;
	});

}

async function setLastConnectionDate(userId, date) {
	const db = await dbService.getConnection();
	const sql = 'UPDATE User SET User.lastConnection = ? WHERE User.id = ?';
	const params = [date, userId];

	try {
		db.query(sql, params);
	}
	catch (err) {
		debug('error during setLastConnectionDate: ', err);
	}
	finally {
		db.release();
	}
}

async function sendMail(to, subject, body) {
	const mailOpts = {
		from: 'matcha@42.fr',
		to,
		subject,
		html: body
	};

	return new Promise((resolve, reject) => {
		mailer.sendMail(mailOpts, (err) => {
			if (err)
				return reject(new Error(err));
			resolve();
		});
	});
}

async function resetPasswd(email, hostname) {
	const db = await dbService.getConnection();
	const code = crypto.generateRandom(32);
	const body = [
		"A password reset was requested.<br>",
		"You can create your new password here: ",
		`http://${hostname}:3000/reset-passwd?code=${code}`, // crados mais osef
		"If you didn't request a new password, just ignore this message."
	].join('<br>');
	const sql = 'UPDATE User SET ? WHERE User.email = ?';
	const user = await getByEmail(email);

	if (!user) {
		console.log('no mail found');
		return ;
	}
	try {
		await db.query(sql, [{resetPasswd: code}, email]);
		return sendMail(user.email, 'Reset password', body);
	}
	finally {
		db.release();
	}
}

async function activate(userId) {
	const db = await dbService.getConnection();
	const sql = 'UPDATE User SET ?';

	try {
		return db.query(sql, [{active: 1}]);
	}
	finally {
		db.release();
	}
}

async function checkExtendedProfile(user) {
	const result = await getImages(user.id);

	if (user.biography === null || result.length < 1)
		return 0;
	return 1;
}

module.exports = {
	getByUsername,
	getByEmail,
	getByPasswdResetCode,
	getById,
	getByActivationCode,
	login,
	menu,
	register,
	updateProfile,
	uploadImage,
	deleteImage,
	loadTags,
	addTag,
	deleteTag,
	editPasswd,
	editEmail,
	getImages,
	addVisit,
	getVisits,
	addLike,
	getLikers,
	unLike,
	getMsg,
	sendMsg,
	getMatches,
	matched,
	isLiked,
	setProfileImage,
	getMatchScore,
	updateLocation,
	getNotifications,
	readNotifications,
	getAll,
	blockUser,
	reportUser,
	getBlockedList,
	listFilter,
	resetPasswd,
	activate,
	setLastConnectionDate,
	checkExtendedProfile
};
