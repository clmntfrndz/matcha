const crypto = require('../crypto.service');
const fs = require('fs');
const moment = require('moment');
const debug = require('debug')('matcha:User.class');

class User {
	constructor(opts) {
		if (typeof opts !== 'object')
			throw new Error(`User constructor expected object, got ${opts}`);

		Object.assign(this, {
			id: opts.id,
			username: opts.username,
			passwdHash: opts.passwdHash,
			email: opts.email,
			birthdate: opts.birthdate,
			gender: opts.gender,
			orientation: opts.orientation,
			city: opts.city,
			firstname: opts.firstname,
			lastname: opts.lastname,
			age: moment().diff(opts.birthdate, 'years'),
			biography: opts.biography,
			popularity: opts.popularity,
			isActive: opts.active,
			level: opts.level,
			lastConnection: opts.lastConnection,
			calcLocation: opts.calcLocation
		});
	}

	async auth(passwd) {
		debug('auth : ', passwd);
		return await crypto.compareHash(passwd, this.passwdHash);
	}

	getImageDir() {
		return `/images/${this.id}/`;
	}

	async createImageDir() {
		return new Promise((resolve, reject) => {
			const path = `${__dirname}/../../public${this.getImageDir()}`;

			fs.mkdir(path, function(err) {
				if (err) {
					if (err.code === `EEXIST`)
						return resolve();
					return reject(err);
				}
				return resolve();
			});
		});
	}

}

module.exports = User;
