const bcrypt = require('bcryptjs');
const crypto = require('crypto');

/**
 * Hash a string using 10 rounds salting
 * @param {string} passwd
 * @returns {Promise<string>}
 */
async function hash(passwd) {
	// Generate salt with 10 rounds
	const salt = await bcrypt.genSalt();

	// Hash password with previously generated salt
	return bcrypt.hash(passwd, salt);
}

/**
 * Compare a string with a hash
 * @param {string} userInput
 * @param {string} hash
 * @returns {Promise<boolean>}
 */
async function compareHash(str, hash) {
	return bcrypt.compare(str, hash);
}

/**
 * Generate `n` random bytes and return result as hexadecimal data
 * @param n Number of random bytes to generate
 * @returns {String}
 */
function generateRandom(n) {
	return crypto.randomBytes(n).toString('hex');
}

// TESTS
// hash('password')
//     .then(function(res) {
//         console.log('"password" hash = ' + res);
//     })
//     .catch(console.error);
//
// compareHash('hello', '$2a$10$e4bEXEq/nt2TMOeHq83isu0rbW3N.h6YIGEfUILHWEjXVrjRVYWKW')
//     .then(function(res) {
//         console.log('incorrect compare res = ' + res);
//     })
//     .catch(console.error);
//
// compareHash('coucou', '$2a$10$e4bEXEq/nt2TMOeHq83isu0rbW3N.h6YIGEfUILHWEjXVrjRVYWKW')
//     .then(function(res) {
//         console.log('correct compare res = ' + res);
//     })
//     .catch(console.error);

module.exports = {
	hash,
	compareHash,
	generateRandom
};
