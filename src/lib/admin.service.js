const dbService = require('./database/database.service');

async function getReportedList() {
	const db = await dbService.getConnection();
	const sql = 'SELECT * FROM `ReportedUser`';

	try {
		return await db.query(sql);
	}

	finally {
		db.release();
	}
}

async function deactivateUser(userId) {
	const db = await dbService.getConnection();
	const sql = 'UPDATE `User` SET active = 0 WHERE id = ?';

	try {
		db.query(sql, [userId]);
	}

	finally {
		db.release();
	}
}

async function deleteReports(userId) {
	const db = await dbService.getConnection();
	const sql = 'DELETE FROM `ReportedUser` WHERE userTo = ?';

	try {
		db.query(sql, [userId]);
	}

	finally {
		db.release();
	}

}

module.exports = {
	getReportedList,
	deactivateUser,
	deleteReports
};