var socket_io = require('socket.io');
var io = socket_io();

let users = {};

io.on('connection', (socket) => {
	// console.log('socket connect event ', socket.request.session);
	if (!socket.request.session.user) {
		console.log('FORBIDDEN');
		return;
	}

	if (!users[socket.request.session.user.id])
		users[socket.request.session.user.id] = {};

	console.log('connect socket id: ' + socket.id);
	users[socket.request.session.user.id][socket.id] = socket;
	// console.log('users = ', users);

	// io.emit(`a new user join the conversation`);

	socket.on('chat message', function(msg) {
		console.log(`message:` + msg);
		io.emit('chat message', msg);
	});

	socket.on('disconnect', () => {
		console.log('disconnect socket id: ' + socket.id);
		delete users[socket.request.session.user.id][socket.id];
	});
});

function getUserSockets(userId) {
	return users[userId];
}

module.exports = {
	io,
	getUserSockets,
};
