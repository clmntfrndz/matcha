/**
 *
 * @type {*}
 */

const express = require('express');
const debug = require('debug')('matcha:app');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const sassMiddleware = require('node-sass-middleware');
const mustacheExpress = require('mustache-express');
const indexRouter = require('./routes/index.router');
const usersRouter = require('./routes/user.router');
const User = require('./lib/user/User.class');
const userService = require('./lib/user/user.service');

// Socket io functions
const socketApi = require('./socketApi');

// Handle sessions
const session = require('express-session')({
	secret: 'cuir cuir',
	resave: true,
	saveUninitialized: true,
});

// Handle upload
const fileUpload = require('express-fileupload');

// Create Express instance
const app = express();

// Express logs using morgan (maybe we should look into "winston")
app.use(logger('dev'));

// Associate .mustache files with mustache
app.engine('mustache', mustacheExpress());

// Use mustache as view engine
app.set('view engine', 'mustache');
app.set('view cache', false);

// Views directory
app.set('views', __dirname + '/views');

// Parse requests with JSON payloads
app.use(express.json());

// Parse requests with url encoded payloads
app.use(express.urlencoded({ extended: false }));

// Parse cookies and populate req.cookies
app.use(cookieParser('cuir cuir'));

// Use session in socket.io
socketApi.io.use((socket, next) => {
	session(socket.request, socket.request.res, next);
});

// Use session
app.use(session);

// Use fileupload
app.use(fileUpload());

// Compile sass to css
app.use(sassMiddleware({
	src: path.join(__dirname, 'public'),
	dest: path.join(__dirname, 'public'),
	indentedSyntax: false, // true = .sass and false = .scss
	sourceMap: true,
}));

// Expose public folder
app.use(express.static(path.join(__dirname, 'public')));

// User reviver + notifications
app.use(async (req, res, next) => {
	if (req.session.user) {
		const me = new User(req.session.user);

		req.session.user = me;
		req.session.notifs = await userService.getNotifications(me.id);
	}
	next();
});

// App routers
app.use('/', indexRouter);

app.use('/user', [enforceLogin, usersRouter]);

app.post('/mail', (req, res, next) => {
	const mailOpts = {
		from: 'matcha@42.fr',
		to: 'clmnt.frndz@gmail.com',
		subject: 'TEST MATCHA',
		html: 'BONJOUR'
	};

	mailer.sendMail(mailOpts, (err, info) => {
		if (err)
			throw new Error(err);
		console.log('infos mail: ', info);
		res.end();
	});
});

function enforceLogin(req, res, next) {
	if (req.session.user)
		return next();
	res.status(401);
	res.send('You need to connect before you accessing this page.');
}

// Error handler
app.use((err, req, res, next) => {
	debug(err);
	res.status(400);
	res.send('Something failed.');
});

debug('App started');

module.exports = app;
