let express = require('express');
let router = express.Router();
let debug = require('debug')('matcha:index');
let crypto = require('../lib/crypto.service');
let moment = require('moment');

const userService = require('../lib/user/user.service');

// TODO: Move to a /lib/database
/* GET home page. */
router.get('/', async (req, res, next) => {
	const me = req.session.user;
	const menu = userService.menu();
	const notifs = req.session.notifs;

	res.render('index', { me, menu, notifs });
});

router.post('/register', async (req, res, next) => {
	let userData = {
		'username': req.body.username,
		'passwd': req.body.passwd,
		'email': req.body.email,
		'gender': req.body.gender,
		'firstname': req.body.firstname,
		'lastname': req.body.lastname,
		'date': req.body.birthdate,
		'activationCode': crypto.generateRandom(20),
		'active': false
	};

	if (req.body.passwd !== req.body.passwd_confirm)
		return res.send(`Confirm password didn't match`);

	try {
		await userService.register(userData, req.hostname);
		res.send(`Check your mails to complete your registration`);
	}
	catch (err) {
		next(err);
	}
});

router.get('/activate', async (req, res, next) => {
	const code = req.query.code;
	let user = await userService.getByActivationCode(code);

	if (!user)
		return res.send('No user associated with this activation code.');
	await userService.activate(user.id);
	res.send('User activated');
});

router.post('/login', async (req, res, next) => {
	if (req.session.user)
		return next('User already connected');
	try {
		req.session.user = await userService.login(req.body.username,
			req.body.passwd);
		debug('user = ', req.session.user);
		debug('imagedir: ', req.session.user.getImageDir());
		if (!req.session.user.isActive) {
			delete req.session.user;
			return res.send('Check your mails to activate your account');
		}
		if (req.session.user) {
			await userService.setLastConnectionDate(req.session.user.id, moment().format('YYYY-MM-DD HH:mm:ss'));
			return res.redirect('/?connect=true');
		}
		return res.send('Wrong username or password');
	}
	catch (err) {
		debug('err login: ', err);
		return res.send('Error during login');
	}
});

/**
 * GET /reset-passwd
 */
router.get('/reset-passwd', async (req, res, next) => {
	const code = req.query.code;

	if (req.session.user)
		return next('User already connected');
	return res.render('reset-passwd', {code});
});

/**
 * POST /reset-password
 */
router.post('/reset-passwd', async (req, res, next) => {
	const email = req.body.email;
	// needed to send mail pointing to correct address
	const hostname = req.hostname;

	await userService.resetPasswd(email, hostname);
	res.send('A password reset link was sent to your email.');
});

/**
 * POST /change-passwd
 */
router.post('/change-passwd', async (req, res, next) => {
	const code = req.body.code;

	if (req.body.passwd !== req.body.passwd_confirm)
		return res.send('Password and confirmation don\'t match');
	const user = await userService.getByPasswdResetCode(code);
	await userService.editPasswd(user.id, req.body.passwd);
	res.send('Password successfully changed');
});

router.post('/logout', async (req, res, next) => {
	if (!req.session.user)
		return res.send(`No user connected, can't disconnect.`);
	delete req.session.user;
	req.session.user = null;
	return res.redirect('/');
});

module.exports = router;
