const express = require('express');
const router = express.Router();
const userService = require('../lib/user/user.service');
const debug = require('debug')('matcha:user.router');
const socketApi = require('../socketApi');
const _ = require('lodash');
const moment = require('moment');
const adminService = require('../lib/admin.service');

// router.use(async (req, res, next) => {
//   req.session.user = await userService.getByUsername('cfz');
//   next();
// });

router.post('/edit', async (req, res, next) => {
	let userData = {
		id: req.session.user.id,
		gender: req.body.gender || req.session.user.gender,
		orientation: req.body.orientation || req.session.user.orientation,
		city: req.body.city || req.session.user.city,
		firstname: req.body.firstname || req.session.user.firstname,
		lastname: req.body.lastname || req.session.user.lastname,
		biography: req.body.biography || req.session.user.biography
	};

	try {
		await userService.updateProfile(userData);
		req.session.user = await userService.getById(req.session.user.id);
		return res.redirect('/user/edit');
	}
	catch (err) {
		return next(err);
	}
});

router.get('/edit', async (req, res, next) => {
	const menu = userService.menu(req.originalUrl);
	const me = req.session.user;
	const images = await userService.getImages(me.id);
	const notifs = req.session.notifs;

	const ui = {
		gender: {
			options: [
				{ text: 'Male', value: 'M' },
				{ text: 'Female', value: 'F' },
				{ text: 'Transexual', value: 'T' },
				{ text: 'Non binary', value: 'X' }
			],
			selected: function() {
				if (this.value === me.gender)
					return 'selected';
				return '';
			}
		},
		orientation: {
			options: [
				{ text: 'Heterosexual', value: 'hetero' },
				{ text: 'Gay', value: 'gay' },
				{ text: 'Bisexual', value: 'bisexual' }
			],
			selected: function() {
				if (this.value === me.orientation)
					return 'selected';
				return '';
			}
		}
	};

	me.images = [];
	for (let i = 0; i < 5; i++) {
		me.images.push({
			path: 'https://bulma.io/images/placeholders/128x128.png',
			imageIndex: i + 1
		});
	}
	images.forEach((image) => {
		me.images[image.imageIndex - 1] = image;
	});

	me.tags = await userService.loadTags(me.id);
	res.render('user/edit', { menu, me, ui, notifs });
});

/**
 * GET /user/matches
 */
router.get('/matches', async (req, res, next) => {
	const menu = userService.menu(req.originalUrl);
	const me = req.session.user;
	const userId = req.session.user.id;
	const notifs = req.session.notifs;
	const extendedProfile = await userService.checkExtendedProfile(me);

	if (!extendedProfile)
		return res.send(`Please fill your profile before`);
	const matches = await userService.getMatches(userId);
	await userService.listFilter(matches, me.id);
	debug('matches = ', matches);
	matches.forEach((match) => {
		match.location = match.calcLocation || match.city;
	});

	res.render('user/matches', { menu, me, matches, notifs });
});

/**
 * GET /user/search
 */
router.get('/search', async (req, res, next) => {
	const menu = userService.menu(req.originalUrl);
	// const me = req.session.user;
	const notifs = req.session.notifs;
	const userList = await userService.getAll();
	const me = await userService.getById(req.session.user.id);
	let suggestions = [];

	let matchScores = [];
	for (let user of userList) {
		let matchScore = userService.getMatchScore(me, user.id)
			.then((score) => {
				if (score)
					suggestions.push(user);
				user.age = moment().diff(user.birthdate, 'years');
				user.matchScore = score;
			});

		matchScores.push(matchScore);
	}

	// Wait for all matchScores to be processed
	await Promise.all(matchScores);

	// check for blocked users
	await userService.listFilter(suggestions, me.id);

	switch (req.query['sort']) {
		case 'age':
			debug('Sort suggestions by age');
			suggestions = _.orderBy(suggestions, [(user) => {
				return new Date(user.birthdate);
			}, 'matchScore.total'], ['asc', 'desc']);
			break;
		case 'location':
			debug('Sort suggestions by location');
			suggestions = _.orderBy(suggestions, ['matchScore.location', 'matchScore.total'], ['desc', 'desc']);
			break;
		case 'popularity':
			debug('Sort suggestions by popularity');
			suggestions = _.orderBy(suggestions, ['matchScore.popularity', 'matchScore.total'], ['desc', 'desc']);
			break;
		case 'interest':
			debug('Sort suggestions by interests');
			suggestions = _.orderBy(suggestions, ['matchScore.interest', 'matchScore.total'], ['desc', 'desc']);
			break;
		default:
			debug('Sort suggestions by total matchScore');
			suggestions = _.orderBy(suggestions, 'matchScore.total', 'desc');
			break;
	}

	if (req.query['min-age']) {
		_.remove(suggestions, (user) => {
			return user.age <= req.query['min-age'];
		});
	}
	if (req.query['max-age']) {
		_.remove(suggestions, (user) => {
			return user.age >= req.query['max-age'];
		});
	}
	if (req.query['min-popularity']) {
		_.remove(suggestions, (user) => {
			return user <= req.query['min-popularity'];
		});
	}
	if (req.query['max-popularity']) {
		_.remove(suggestions, (user) => {
			return user >= req.query['max-popularity'];
		});
	}
	if (req.query['location']) {
		_.remove(suggestions, (user) => {
			let location = user.calcLocaton || user.city;

			return location.toLowerCase() !== req.query['location'];
		});
	}
	if (req.query['tag']) {
		_.remove(suggestions, async (user) => {
			let tags = await userService.loadTags(user.id);
			debug('tags for user: ' + user.id, tags);

			return _.find(user.tags, { tagName: req.query['tag'] });
		});
	}


	debug('req params', req.query);
	res.render('user/search', { menu, me, notifs, suggestions });
});

/**
 * GET /user/visits
 */
router.get('/visits', async (req, res, next) => {
	const menu = userService.menu(req.originalUrl);
	const me = req.session.user;
	const visits = await userService.getVisits(me.id);
	const notifs = req.session.notifs;
	const extendedProfile = await userService.checkExtendedProfile(me);

	if (!extendedProfile)
		return res.send(`Please fill your profile before`);
	// check for blocked users
	await userService.listFilter(visits, me.id);
	debug('visits = ', visits);
	res.render('user/visits', { menu, me, visits, notifs });
});

/**
 * POST /user/tags/add
 */
router.post('/tags/add', async (req, res, next) => {
	debug('req body = ', req.body);
	const me = req.session.user;
	const tag = {
		name: req.body.tag,
		userId: me.id
	};

	try {
		await userService.addTag(tag);
		res.redirect(`${req.baseUrl}/edit`);
	} catch (err) {
		if (err.code === 'ER_DUP_ENTRY')
			res.send(`Tag already exists`);
		else {
			debug('Err caught in /tags/add', err);
			next(err);
		}
	}
});

/**
 * POST /user/tags/delete
 */
router.post('/tags/delete', async (req, res, next) => {
	debug('req body = ', req.body);
	const user = req.session.user;
	const tag = {
		userTagId: req.body.userTagId,
		userId: req.session.user.id
	};

	try {
		await userService.deleteTag(tag);
		debug('delete done, redirecting now');
		res.status(200).end();
	} catch (err) {
		debug('Err caught in /tags/delete', err);
		next(err);
	}
});

/**
 * GET /user/:id
 * @description Access to a user profile page by its id, internally increment the visit counter
 */
router.get('/:userId(\\d+)', async (req, res, next) => {
	const menu = userService.menu(req.originalUrl);
	const user = await userService.getById(req.params.userId);
	const me = req.session.user;
	const notifs = req.session.notifs;
	const sockets = socketApi.getUserSockets(user.id);


	// check for extendedProfile
	const extendedProfile = await userService.checkExtendedProfile(me);
	if (!extendedProfile)
		return res.send(`Please fill your profile before`);
	// check for blocked users
	const blockedList = await userService.getBlockedList(me.id);
	if (blockedList.indexOf(user.id) !== -1)
		return res.send(`User blocked`);
	await userService.addVisit(me.id, user.id);
	user.tags = await userService.loadTags(user.id);
	user.images = await userService.getImages(user.id);
	await userService.getMatchScore(me.id, user.id);
	user.matched = await userService.matched(me.id, user.id);
	user.liked = await userService.isLiked(me.id, user.id);
	user.connected = sockets ? !!Object.keys(sockets).length : false;
	debug('user = ', user);
	res.render('user/profile', { menu, user, me, notifs });
});

/**
 * GET /user/me
 * @description Access to my profile page (alias of /user/<my_user_id>
 */
router.get('/me', async (req, res, next) => {
	const menu = userService.menu(req.originalUrl);
	const me = req.session.user;
	const user = await userService.getById(me.id);
	const notifs = req.session.notifs;

	user.tags = await userService.loadTags(user.id);
	user.images = await userService.getImages(user.id);
	// Mark user as "me" so that we can't like ourselves
	user.me = true;
	res.render('user/profile', { menu, user, me, notifs });
});

router.post(`/image/upload`, async (req, res, next) => {
	debug('files = ', req.files);
	debug('body = ', req.body);
	if (req.files.upload.mimetype === `image/gif` || req.files.upload.mimetype ===
		`image/png` || req.files.upload.mimetype === `image/jpeg`) {
		const path = `${__dirname}/../public${req.session.user.getImageDir()}`;
		const filename = Date.now() + '_' + req.files.upload.name;
		const image = {
			path: req.session.user.getImageDir() + filename,
			imageIndex: req.body.imageIndex,
			userId: req.session.user.id
		};

		debug('path = ', image);

		try {
			await req.session.user.createImageDir();
			await req.files.upload.mv(path + filename);
			await userService.uploadImage(image);
			return res.redirect(`../edit`);
		}
		catch (err) {
			next(err);
		}
	}
	return res.send('Please select a valid picture');
});

router.post(`/image/delete`, async (req, res, next) => {
	req.session.user = await userService.getByUsername(`cfz`);
	if (!req.body.imageIndex) {
		return res.render(`Specified imageIndex is not correct`);
	}
	else {
		try {
			let sth = await userService.deleteImage(req.session.user.id,
				req.body.imageIndex);

			if (sth.affectedRows)
				return res.send(`Image deleted`);
			else
				return res.send(`Cannot delete the image, is it set as profile?`);
		}
		catch (err) {
			return next(err);
		}
	}
});

// Edit email and passwd
router.post(`/edit/passwd`, async (req, res, next) => {
	if (req.body.new_passwd !== req.body.confirm_passwd)
		res.send(`Confirm password didn't match`);
	else {
		if (await req.session.user.auth(req.body.old_passwd)) {
			try {
				await userService.editPasswd(req.session.user.id, req.body.new_passwd);
				return res.send(`Password updated`);
			}
			catch (err) {
				return next(err);
			}
		}
		else
			return res.send(`Wrong password`);
	}
});

router.post(`/edit/email`, async (req, res, next) => {
	if (!req.body.new_email)
		return res.send(`Specified email is not correct`);
	else
		try {
			await userService.editEmail(req.session.user.email, req.body.new_email);
			return res.send(`Email updated`);
		}
		catch (err) {
			return next(err);
		}
});

router.post(`/visits/add`, async (req, res, next) => {
	if (!req.body.userFrom || !req.body.userTo) {
		return res.send(`Missing parameter from or to`);
	}
	else {
		try {
			await userService.addVisit(req.body.userFrom, req.body.userTo);
			return res.send(`Visit added / updated`);
		}
		catch (err) {
			return next(err);
		}
	}
});

router.get(`/likes`, async (req, res, next) => {
	const menu = userService.menu(req.originalUrl);
	const me = req.session.user;
	const likes = await userService.getLikers(me.id);
	const notifs = req.session.notifs;
	const extendedProfile = await userService.checkExtendedProfile(me);

	if (!extendedProfile)
		return res.send(`Please fill your profile before`);
	// check for blocked users
	await userService.listFilter(likes, me.id);
	debug('likes = ', likes);
	likes.forEach((like) => {
		like.location = like.calcLocation || like.city;
	});
	res.render('user/likes', { menu, me, likes, notifs });
});

router.post(`/likes/add`, async (req, res, next) => {
	const me = req.session.user;

	if (!req.body.userTo)
		return res.send(`Missing parameter userTo`);
	else {
		try {
			await userService.addLike(me.id, req.body.userTo);
			return res.send(`Like added / updated`);
		}
		catch (err) {
			return next(err);
		}
	}
});

router.post(`/likes/unlike`, async (req, res, next) => {
	const me = req.session.user;

	if (!req.body.userTo)
		return res.send(`Missing parameter userTo`);
	else {
		try {
			const sth = await userService.unLike(me.id, req.body.userTo);
			if (sth.affectedRows)
				return res.send(`Successfully unlike the user`);
			else
				return res.send(`Cannot unlike a non-liked user`);
		}
		catch (err) {
			return next(err);
		}
	}
});

router.get(`/chat/:id(\\d+)`, async (req, res, next) => {
	const me = req.session.user;
	// check for blocked users
	const blockedList = await userService.getBlockedList(me.id);
	if (blockedList.indexOf(parseInt(req.params.id)) !== -1)
		return res.send(`User blocked`);
	const msg = await userService.getMsg(req.session.user.id, req.params.id);
	const matched = await userService.matched(req.session.user.id, req.params.id);
	const notifs = req.session.notifs;

	if (!matched)
		return res.redirect('../../user/matches');
	console.log('cookies from req: ', req.cookies);
	console.log('session  ', req.session);

	res.render('user/chat', { msg, me, notifs });
});

router.post('/chatmsg', async (req, res, next) => {
	const userSockets = await socketApi.getUserSockets(req.body.userTo);
	const msg = req.body.message.trim();
	const matched = await userService.matched(req.session.user.id,
		req.body.userTo);

	if (!matched)
		return res.end();
	if (!msg)
		return res.send('Error! please mention a message for whisper');
	debug('user sockets = ', userSockets);
	try {
		userService.sendMsg(req.session.user.id, req.body.userTo, msg);
		_.forEach(userSockets, (socket, id) => {
			debug('send msg to socket id ' + id);
			socket.emit('chat message', {
				message: msg,
				fromUsername: req.session.user.username
			});
		});
	}
	catch (err) {
		return next(err);
	}
	res.end();
});

router.post('/image/set-profile', async (req, res, next) => {
	const userId = req.session.user.id;
	const imageIndex = req.body['image-index'];

	debug('req body = ', req.body);
	await userService.setProfileImage(userId, imageIndex);
	res.redirect('/user/edit');
});

router.post('/update-location', async (req, res, next) => {
	const userId = req.session.user.id;
	const latLng = {
		'lat': req.body.lat,
		'lng': req.body.lng
	};
	const locFromLatLng = req.body.location;

	await userService.updateLocation(userId, latLng, locFromLatLng);
	res.send('OK');
});

router.post('/notif/read-all', async (req, res, next) => {
	const me = req.session.user;

	await userService.readNotifications(me.id);
	res.send({ notifs: [] });
});

router.get('/block', async (req, res, next) => {
	const userTo = parseInt(req.query.id);
	const me = req.session.user;
	const blockedList = await userService.getBlockedList(me.id);

	if (blockedList.indexOf(userTo) !== -1)
		return res.send(`User already blocked`);
	await userService.blockUser(me.id, userTo);
	return res.redirect('/');
});

router.get('/report', async (req, res, next) => {
	const userTo = parseInt(req.query.id);
	const me = req.session.user;
	const blockedList = await userService.getBlockedList(me.id);

	// check for blocked users
	if (blockedList.indexOf(userTo) !== -1)
		return res.send(`User already reported`);
	await userService.reportUser(userTo);
	await userService.blockUser(me.id, userTo);
	return res.redirect('/');
});

router.get('/admin-panel', async (req, res, next) => {
	const menu = userService.menu(req.originalUrl);
	const me = req.session.user;
	const notifs = req.session.notifs;

	if (req.session.user.level === 0)
		return res.redirect('/');
	if (req.query.id || req.query.action) {
		if (req.query.action === 'deactivate')
			await adminService.deactivateUser(req.query.id);
		if (req.query.action === 'reset')
			await adminService.deleteReports(req.query.id);
	}
	const list = await adminService.getReportedList();
	res.render('user/admin', { menu, me, list, notifs });
});



module.exports = router;
