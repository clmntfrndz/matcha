FROM		node:8

RUN         apt-get update && apt-get install -y \
                ssmtp
RUN			npm install -g nodemon@1.15.1

# Source code
RUN         mkdir /app
WORKDIR		/app
COPY        package*.json ./
RUN			npm install --only=production
COPY        nodemon.json ./

# Mail settings
COPY    sendmail_path.ini   /usr/local/etc/php/conf.d/sendmail_path.ini
COPY    ssmtp.conf          /etc/ssmtp/ssmtp.conf

EXPOSE      3000

CMD         npm start
